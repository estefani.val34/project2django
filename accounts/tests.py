"""
Test
Contains :

1. **MySeleniumTests** - Automatic login test (2 test).
2. **SimpleTest** - Client Test (1 test).



References:

Django: https://docs.djangoproject.com/en/3.0/topics/testing/tools/

Automatic Test : https://www.selenium.dev/

Command Run Tests : ./manage.py test proteins.tests

Status Codes: https://www.django-rest-framework.org/api-guide/status-codes/
"""

import pytest
from django.core.management import call_command
from django.test import TestCase
from django.test import Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# === MySeleniumTests ===
class MySeleniumTests(StaticLiveServerTestCase):
    """
    Automatically open Firefox then go to the pre-assign page, enter the
    credentials and press the “create” button.
    """
  
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_preasign(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/preasign'))
        ega_box_number_input = self.selenium.find_element_by_name("ega_box_number")
        ega_box_number_input.send_keys('lalalala')
        form_email_input = self.selenium.find_element_by_name("form-0-email")
        form_email_input.send_keys('eeee@gmail.es')
        self.selenium.find_element_by_xpath('//button[@value="create"]').click()
    
    def test_assign(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/find'))
        ega_box_input = self.selenium.find_element_by_name("ega_box")
        ega_box_input.send_keys('11')
        self.selenium.find_element_by_xpath('//button[@value="Search"]').click()


# === SimpleTest ===
class SimpleTest(TestCase):
    """
    Client Test
    """
    def test_list(self):
        response = self.client.get('/all')
        self.assertEqual(response.status_code, 200)
