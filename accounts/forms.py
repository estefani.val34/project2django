"""
Forms that are used in views.py.
Contains 7 forms:

1. **EgaBoxNumbersAvailableModelForm** - Form used in function change_number_available of views.py
2. **SubmissionAccountsModelForm** - Form used in function create_account of views.py.
3. **CountryFormset** - Form used in function create_account of views.py.
4. **FindSubmissionAccountForm** - Form used in function find_account of views.py.
5. **ModifySubBoxesModelForm** - Form used in function modify_submissionboxes of views.py.
6. **SubmissionBoxesModelForm** - Form used in function create_submissionboxes_with_users of views.py.
7. **UserFormset** - Form used in function create_submissionboxes_with_users of views.py.

"""


from django import forms
from django.forms import (formset_factory, modelformset_factory)
from .models import (
    SubmissionBoxes,
    User,
    SubmissionAccounts,
    Country,
    EgaBoxNumbersAvailable)

from django.forms import ComboField


FAVORITE_SUBMISSIONTYPE_CHOICES = [
    ('sequence', 'Sequence'),
    ('array', 'Array'),
    ('phenotype', 'Phenotype'),
]

# === EgaBoxNumbersAvailableModelForm ===
class EgaBoxNumbersAvailableModelForm(forms.ModelForm):
    ega_maxim = forms.IntegerField(required=True)

    class Meta:
        model = EgaBoxNumbersAvailable
        fields = ('ega_maxim',)

    def clean(self):
        cleaned_data = super().clean()
        ega_maxim = cleaned_data.get("ega_maxim")

        numbers_maxims = list(EgaBoxNumbersAvailable.objects.all(
        ).values_list('ega_maxim', flat=True))
        if len(numbers_maxims) > 0:
            maxi_num = max(numbers_maxims)
        else:
            maxi_num = 0

        if ega_maxim < maxi_num:
            msg = "Please select a value that is not less than "+str(maxi_num)
            self.add_error('ega_maxim', msg)


# === SubmissionAccountsModelForm ===
class SubmissionAccountsModelForm(forms.ModelForm):
    submission_type = forms.MultipleChoiceField(
        required=True,
        widget=forms.CheckboxSelectMultiple,
        choices=FAVORITE_SUBMISSIONTYPE_CHOICES,
    )

    class Meta:
        model = SubmissionAccounts
        fields = ('__all__')
        exclude = ['country', 'mapi', 'affiliation_project',
                   'account_opened_date', 'submission_box']


# === CountryFormset ===
CountryFormset = modelformset_factory(
    Country,
    fields=('name', ),
    extra=0,
    min_num=1, validate_min=True,
    widgets={'name': forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter Country Name here'
    })
    }
)


# === FindSubmissionAccountForm ===
class FindSubmissionAccountForm(forms.Form):
    ega_box = forms.IntegerField(required=True)

# === ModifySubBoxesModelForm ===


class ModifySubBoxesModelForm(forms.ModelForm):

    class Meta:
        model = SubmissionBoxes
        fields = ('ega_box_number', 'rt_ticket', 'emails',
                  'received_dta', 'comments')

# === SubmissionBoxesModelForm ===


class SubmissionBoxesModelForm(forms.ModelForm):

    class Meta:
        model = SubmissionBoxes
        fields = ('ega_box_number', 'rt_ticket', 'comments')
        labels = {
            'ega_box_number': 'Ega Box Number',
            'rt_ticket': 'Rt Ticket',
            'comments': 'Comments'
        }
        widgets = {
            'ega_box_number': forms.NumberInput(attrs={
                'class': 'form-control',
                'placeholder': '1234'
            }
            ),
            'rt_ticket': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter rt ticket here'
            }
            ),
            'comments': forms.Textarea(attrs={
                'class': 'form-control'
            }
            )
        }

    def clean(self):
        cleaned_data = super().clean()
        ega_box_number = cleaned_data.get("ega_box_number")

        numbers_maxims = list(EgaBoxNumbersAvailable.objects.all(
        ).values_list('ega_maxim', flat=True))
        if len(numbers_maxims) > 0:
            maxi_num = max(numbers_maxims)
        else:
            maxi_num = 0

        if ega_box_number > maxi_num:
            msg = "Number not available."
            self.add_error('ega_box_number', msg)


# === UserFormset ===
UserFormset = modelformset_factory(
    User,
    fields=('email', ),
    extra=0,
    min_num=1, validate_min=True,
    widgets={
        'email': forms.EmailInput(attrs={
            'class': 'form-control',
            'placeholder': 'Enter email here'
        }
        )
    }

)
